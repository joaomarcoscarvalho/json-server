var cors = require("cors");
const jsonServer = require("json-server");
const auth = require("json-server-auth");

const server = jsonServer.create();
const router = jsonServer.router("db.json");
const port = process.env.PORT || 3001;

server.db = router.db;

const rules = auth.rewriter({
  "users*": "/600/users$1",
  "/residents*": "/644/residents$1",
  "/status*": "/640/status$1",
});

server.use(cors());
server.use(rules);
server.use(auth);
server.use(router);

server.listen(port, () => {
  console.log("JSON Server is running");
});
